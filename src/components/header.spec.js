import * as React from 'react'
import renderer from 'react-test-renderer'

import Header from './header'

test('Header', () => {
  const tree = renderer
    .create(<Header siteTitle="Default Starter" />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
